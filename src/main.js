import Vue from 'vue'
import App from './App.vue'
import store from './store'
import { BootstrapVue } from 'bootstrap-vue'
import VueRamda from 'vue-ramda'

import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faHome,
  faSpinner,
  faFire,
  faSearch,
  faUserCircle,
  faBell,
  faTh,
  faVideo,
  faFilm,
  faHistory,
  faLayerGroup,
  faClock,
  faAngleDown,
  faCaretDown,
  faSortAmountDown,
  faEllipsisV,
  faCheckCircle
} from '@fortawesome/free-solid-svg-icons'
import { faYoutube } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
const icons = [
  faHome,
  faSpinner,
  faFire,
  faYoutube,
  faSearch,
  faUserCircle,
  faBell,
  faTh,
  faVideo,
  faFilm,
  faHistory,
  faLayerGroup,
  faClock,
  faAngleDown,
  faCaretDown,
  faSortAmountDown,
  faEllipsisV,
  faCheckCircle
]

library.add(...icons)
Vue.component('fa', FontAwesomeIcon)

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(VueRamda)

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
