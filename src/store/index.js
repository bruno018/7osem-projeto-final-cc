import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    videos: [
      {
        kind: 'youtube#video',
        etag: 'yT8NriM9Fv3fl_KGp9aWtWW2jsA',
        id: '-5q5mZbe3V8',
        snippet: {
          publishedAt: '2020-11-20T04:58:11Z',
          channelId: 'UC3IZKseVpdzPSBaWxBxundA',
          title: "BTS (방탄소년단) 'Life Goes On' Official MV",
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/-5q5mZbe3V8/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Big Hit Labels',
          categoryId: '10',
          liveBroadcastContent: 'none',
          defaultLanguage: 'ko',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 109903506,
          likeCount: 10421386,
          dislikeCount: 65014,
          favoriteCount: 0,
          commentCount: 3801684
        }
      },
      {
        kind: 'youtube#video',
        etag: '_MvhEqWQRHP_Pnof3Tx2--E34KQ',
        id: 'v4yqcI8HinA',
        snippet: {
          publishedAt: '2020-11-11T16:50:58Z',
          channelId: 'UCPpATKqmMV-CNRNWYaDUwiA',
          title: 'We Broke Up',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/v4yqcI8HinA/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Alexa Rivera',
          categoryId: '26',
          liveBroadcastContent: 'none'
        },
        statistics: {
          viewCount: 1929905,
          likeCount: 187614,
          dislikeCount: 8129,
          favoriteCount: 0,
          commentCount: 37685
        }
      },
      {
        kind: 'youtube#video',
        etag: '3PZc-BVwT4pMvM6tYsP70d_tPXM',
        id: 'm-_GFcWr0lI',
        snippet: {
          publishedAt: '2020-07-10T14:25:41Z',
          channelId: 'UC3sznuotAs2ohg_U__Jzj_Q',
          title: 'Film Theory: The Lorax Movie LIED To You!',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/m-_GFcWr0lI/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'The Film Theorists',
          categoryId: '1',
          liveBroadcastContent: 'none',
          defaultLanguage: 'en',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 1734022,
          likeCount: 105377,
          dislikeCount: 4673,
          favoriteCount: 0,
          commentCount: 11649
        }
      },
      {
        kind: 'youtube#video',
        etag: 'b7bMkGfPcLJ9PY_hR4ZbN0LEzQg',
        id: 'H0_mdJrnMds',
        snippet: {
          publishedAt: '2020-10-21T21:47:18Z',
          channelId: 'UCepPGz8AVCbggMl3BvboaBA',
          title: 'Brewstew - Wrecking My First Car',
          hasCC: true,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/H0_mdJrnMds/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'brewstewfilms',
          categoryId: '23',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 401482,
          likeCount: 35484,
          dislikeCount: 224,
          favoriteCount: 0,
          commentCount: 3490
        }
      },
      {
        kind: 'youtube#video',
        etag: 'CslJTODYQG2aea4ZqQ0iS7HmlWw',
        id: 'mt7GmDhvzg4',
        snippet: {
          publishedAt: '2020-11-25T09:07:14Z',
          channelId: 'UCDQBZcjYKP1J1Nu-Y0_D37Q',
          title: 'BREAKING $100 WORTH OF COOKIES (Roommate Stories)',
          hasCC: true,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/mt7GmDhvzg4/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Tabbes',
          categoryId: '1',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 586552,
          likeCount: 76408,
          dislikeCount: 357,
          favoriteCount: 0,
          commentCount: 3969
        }
      },
      {
        kind: 'youtube#video',
        etag: 'aWHdZteQ4SYEnFYmjA58I-ZNHvU',
        id: '7PBYGu4Az8s',
        snippet: {
          publishedAt: '2020-01-20T05:00:10Z',
          channelId: 'UCKrdjiuS66yXOdEZ_cOD_TA',
          title: 'Megan Thee Stallion - Body [Official Video]',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/7PBYGu4Az8s/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Megan Thee Stallion',
          categoryId: '10',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 8139716,
          likeCount: 424830,
          dislikeCount: 20932,
          favoriteCount: 0,
          commentCount: 26033
        }
      },
      {
        kind: 'youtube#video',
        etag: 'vi9MRl8QssgTFpRBEO_VoVY474s',
        id: 'POhHpDMWofc',
        snippet: {
          publishedAt: '2019-11-17T16:30:05Z',
          channelId: 'UCGCVyTWogzQ4D170BLy2Arw',
          title: 'Calling Out Companies Who Stole My Nails',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/POhHpDMWofc/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Simply Nailogical',
          categoryId: '24',
          liveBroadcastContent: 'none',
          defaultLanguage: 'en',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 929613,
          likeCount: 77582,
          dislikeCount: 754,
          favoriteCount: 0,
          commentCount: 5683
        }
      },
      {
        kind: 'youtube#video',
        etag: 'baff_zY_gEEboYGqWxiZoWuBq60',
        id: '6FrQfONE5zk',
        snippet: {
          publishedAt: '2017-07-02T06:16:05Z',
          channelId: 'UCYUbNjkuE4lsr2v1Id2O1oA',
          title: 'Florida man wrestles puppy from jaws of alligator',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/6FrQfONE5zk/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'ABC7 News Bay Area',
          tags: ['news'],
          categoryId: '25',
          liveBroadcastContent: 'none'
        },
        statistics: {
          viewCount: 563419,
          likeCount: 12220,
          dislikeCount: 260,
          favoriteCount: 0,
          commentCount: 3033
        }
      },
      {
        kind: 'youtube#video',
        etag: 'NBLzysEQ6Ev36TbMuj_Yz8-S5rY',
        id: 'uKhy1Y69BT4',
        snippet: {
          publishedAt: '2020-10-08T11:10:58Z',
          channelId: 'UC2CCXzC56k0b8L49R_iW-Yw',
          title: 'DaBaby - Gucci Peacoat (Official Video)',
          hasCC: true,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/uKhy1Y69BT4/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'DaBaby',
          tags: ['DaBaby', 'Baby Jesus', 'Charlotte', 'Hip Hop', 'Rap'],
          categoryId: '10',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 2329474,
          likeCount: 200970,
          dislikeCount: 2319,
          favoriteCount: 0,
          commentCount: 11278
        }
      },
      {
        kind: 'youtube#video',
        etag: 'MHi7Etc_yEpJd5H-O3fvFHGi7HA',
        id: 'E0gmyePNri4',
        snippet: {
          publishedAt: '2020-11-20T05:00:06Z',
          channelId: 'UC9HlWVtENbiMQzVRmnbJxtw',
          title: 'Meek Mill - Pain Away feat. Lil Durk [Official Video]',
          hasCC: true,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/E0gmyePNri4/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Meek Mill',
          categoryId: '10',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 3320165,
          likeCount: 148613,
          dislikeCount: 3166,
          favoriteCount: 0,
          commentCount: 9238
        }
      },
      {
        kind: 'youtube#video',
        etag: 'xe5NWTjhFWHgR2s-LWW54j4zqOk',
        id: 'cR9_5dYNw_U',
        snippet: {
          publishedAt: '2020-11-21T23:03:06Z',
          channelId: 'UCwVg9btOceLQuNCdoQk9CXg',
          title: 'telling our friends we broke up...',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/cR9_5dYNw_U/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Ben Azelart',
          categoryId: '24',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 1481823,
          likeCount: 152061,
          dislikeCount: 6009,
          favoriteCount: 0,
          commentCount: 35486
        }
      },
      {
        kind: 'youtube#video',
        etag: '9HN7emYQU5Zc68_Mg5z8VbNUhfE',
        id: 'XwxdwVUIMzc',
        snippet: {
          publishedAt: '2020-11-21T15:00:45Z',
          channelId: 'UCAfcdqiPUDqIr3Pv_AGIjZQ',
          title: "JH Diesel Flipped His MegaTruck Into Cleetus' Pond...Now We See If It Runs! (6.0 Powerstroke)",
          hasCC: true,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/XwxdwVUIMzc/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Stapleton42',
          categoryId: '2',
          liveBroadcastContent: 'none'
        },
        statistics: {
          viewCount: 280293,
          likeCount: 8464,
          dislikeCount: 136,
          favoriteCount: 0,
          commentCount: 674
        }
      },
      {
        kind: 'youtube#video',
        etag: 'S4ISNt7nCedkweIxu9TALHZOpMc',
        id: '3F0eyrZhuHs',
        snippet: {
          publishedAt: '2020-04-05T23:39:59Z',
          channelId: 'UChDKyKQ59fYz3JO2fl0Z6sg',
          title: 'Kyle Rittenhouse Posts Bail With Help From My Pillow Founder Michael Lindell | TODAY',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/3F0eyrZhuHs/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'TODAY',
          categoryId: '25',
          liveBroadcastContent: 'none',
          defaultLanguage: 'en',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 1618564,
          likeCount: 16804,
          dislikeCount: 8148,
          favoriteCount: 0,
          commentCount: 30682
        }
      },
      {
        kind: 'youtube#video',
        etag: 'ZJ59eeDZrAVsmOYqxk8D5P0IWQE',
        id: '4KiFSKLTj0U',
        snippet: {
          publishedAt: '2020-07-18T16:30:06Z',
          channelId: 'UCKTgj8r-wp80wJgDxhc6jpA',
          title: 'Carrie Underwood & John Legend - Hallelujah (Official Music Video)',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/4KiFSKLTj0U/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'carrieunderwoodVEVO',
          categoryId: '10',
          liveBroadcastContent: 'none'
        },
        statistics: {
          viewCount: 934392,
          likeCount: 22715,
          dislikeCount: 1119,
          favoriteCount: 0,
          commentCount: 1808
        }
      },
      {
        kind: 'youtube#video',
        etag: 'LtE0o3AyZ7HUKh-LAPeI9d5QzcU',
        id: 'Hfti3BdSqZI',
        snippet: {
          publishedAt: '2020-04-07T06:51:45Z',
          channelId: 'UCvfat4DPrM3BM-xZ2zn5ruQ',
          title: 'NoCap - Pain Show (Official Music Video)',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/Hfti3BdSqZI/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'NoCap',
          categoryId: '10',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 477711,
          likeCount: 32262,
          dislikeCount: 429,
          favoriteCount: 0,
          commentCount: 2220
        }
      },
      {
        kind: 'youtube#video',
        etag: 'BNKSLbeOLCOqvCINAkKq3p8qLes',
        id: 'MPbUaIZAaeA',
        snippet: {
          publishedAt: '2020-09-01T21:04:50Z',
          channelId: 'UC4-TgOSMJHn-LtY4zCzbQhw',
          title: 'Shawn Mendes, Justin Bieber - Monster',
          hasCC: true,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/MPbUaIZAaeA/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'ShawnMendesVEVO',
          categoryId: '10',
          liveBroadcastContent: 'none'
        },
        statistics: {
          viewCount: 20756475,
          likeCount: 1909234,
          dislikeCount: 27791,
          favoriteCount: 0,
          commentCount: 114976
        }
      },
      {
        kind: 'youtube#video',
        etag: 'zwa-aLtWxonsPR0jmKaC2kiNr4Y',
        id: 'sGz5FZsitXk',
        snippet: {
          publishedAt: '2020-07-04T14:06:08Z',
          channelId: 'UC8CbFnDTYkiVweaz8y9wd_Q',
          title: "Mace Windu's Return in The Mandalorian Season 2 - Star Wars Theory",
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/sGz5FZsitXk/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Star Wars Theory',
          categoryId: '24',
          liveBroadcastContent: 'none',
          defaultLanguage: 'en',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 656844,
          likeCount: 28746,
          dislikeCount: 920,
          favoriteCount: 0,
          commentCount: 3818
        }
      },
      {
        kind: 'youtube#video',
        etag: 'RGKXJ77LnNLeVLHZ2PK7FWn3gTU',
        id: 'tXSnd7zJlpU',
        snippet: {
          publishedAt: '2020-11-20T14:33:02Z',
          channelId: 'UChi08h4577eFsNXGd3sxYhw',
          title: 'Jeezy On Ending Feud With Gucci Mane, Black Men Healing, New Album + More',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/tXSnd7zJlpU/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'Breakfast Club Power 105.1 FM',
          categoryId: '24',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 1048714,
          likeCount: 22401,
          dislikeCount: 764,
          favoriteCount: 0,
          commentCount: 6510
        }
      },
      {
        kind: 'youtube#video',
        etag: 'HM3djzio_ei22u9cGcocmQ36QnY',
        id: 'Es0wz5-tzjg',
        snippet: {
          publishedAt: '2020-11-13T00:37:42Z',
          channelId: 'UCZ3pVT4hv4hgWwHpJHyTNRQ',
          title: 'Los Dos Carnales - El Borracho (Video Oficial)',
          hasCC: true,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/Es0wz5-tzjg/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'AfinArte Music',
          categoryId: '10',
          liveBroadcastContent: 'none',
          localized: {
            title: 'Los Dos Carnales - El Borracho (Video Oficial)',
            hasCC: true,
            description:
              '#losdoscarnales  #afinartemusic\nSuscríbete aquí: http://bit.ly/2LWw4Ct\nContrataciones: Miguel (714) 277-5589\nLos Dos Carnales - El Borracho (Video Oficial)\n\nLocalizada en Anaheim California, AfinArte Music es una disquera que desarrolla talento musical, representación, promocional y editorial.\n\nSiguenos en:\nInstagram: @afinartemusic \nLike en FB: /Afinartemusic/'
          },
          defaultAudioLanguage: 'es'
        },
        statistics: {
          viewCount: 1428195,
          likeCount: 47511,
          dislikeCount: 914,
          favoriteCount: 0,
          commentCount: 1502
        }
      },
      {
        kind: 'youtube#video',
        etag: 'lhZmpQmbqs1A0MyeFEQRGQ4u1x0',
        id: 'kc4j59C_60E',
        snippet: {
          publishedAt: '2020-02-14T17:08:01Z',
          channelId: 'UC9MAhZQQd9egwWCxrwSIsJQ',
          title: 'The San Andreas Fault: Disaster About to Strike | How the Earth Was Made | Full Episode | History',
          hasCC: false,
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/kc4j59C_60E/default.jpg',
              width: 120,
              height: 90
            }
          },
          channelTitle: 'HISTORY',
          categoryId: '24',
          liveBroadcastContent: 'none',
          defaultAudioLanguage: 'en'
        },
        statistics: {
          viewCount: 139580,
          likeCount: 3037,
          dislikeCount: 141,
          favoriteCount: 0,
          commentCount: 494
        }
      }
    ],
    channelInfo: {
      name: 'Charlinhos Channel',
      subscribersCount: 3711322,
      avatar: 'channelLogo.png',
      subscribedAt: [
        {
          avatar:
            'https://nice-assets.s3-accelerate.amazonaws.com/smart_templates/411661001fea59f195b0b7eefd9b8db8/assets/preview_7bee0fd06aca1b8e04c77bb0087be8e0.jpg',
          channelName: 'Distinta Game'
        },
        {
          avatar: 'https://www.meme-arsenal.com/memes/2b524eb4ecb21f138223e56cf840d0c3.jpg',
          channelName: 'Originais Film'
        },
        {
          avatar: 'https://cdn.dribbble.com/users/5136162/screenshots/14173991/shot-cropped-1599718909673.png',
          channelName: 'Sagaz Development'
        },
        {
          avatar: 'https://media.moddb.com/images/members/4/3114/3113314/new_youtube_avatar_dark_copy.jpg',
          channelName: 'Leves Design'
        },
        {
          avatar: 'https://miro.medium.com/max/2560/1*ahpxPO0jLGb9EWrY2qQPhg.jpeg',
          channelName: 'JavascriptWorld'
        },
        {
          avatar: 'https://jacket6-channel.weebly.com/uploads/2/8/2/9/28296153/s536731752561537107_p2_i2_w500.jpeg',
          channelName: 'LedJet'
        }
      ]
    }
  },
  getters: {
    getAllVideos: state => state.videos,
    getChannelInfo: state => state.channelInfo,
    getSubscriptionList: state => state.channelInfo.subscribedAt
  },
  mutations: {},
  actions: {},
  modules: {}
})
