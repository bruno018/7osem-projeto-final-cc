# 7osem-projeto-final-cc
### Bruno Tácio Cachoeira               RA: 709123-4                        
### Claudio Vinícius da Silva Figuerôa  RA: 709119-2                        
### Douglas André Oliveira Paiva        RA: 709296-8                        
### Leandro Frederico                   RA: 709069-9                        


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
